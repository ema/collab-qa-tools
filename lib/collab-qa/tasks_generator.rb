=begin

ruby -Ilib -rcollab-qa -e "
tg = CollabQA::TasksGenerator::new ;
t1 = tg.generate({'type' => 'rebuild-full', 'exclude-not-testing' => true, 'include' => 'to_rebuild'}) ;
puts JSON::pretty_generate(t1)" > tasks.json


ruby -Ilib -rcollab-qa -e "
tg = CollabQA::TasksGenerator::new ;
t14 = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'modes' => ['clang14'], 'id' => 'clang14'}) ;
tu = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'id' => 'binarchonly'}) ;
puts JSON::pretty_generate(t14 + tu)" > tasks.json

ruby -Ilib -rcollab-qa -e "
tg = CollabQA::TasksGenerator::new ;
t1 = tg.generate({'type' => 'rebuild-full', 'exclude-not-testing' => true, 'include' => 'sphinx_and_docutils.txt', 'id' => 'sphinx-ref' }) ;
t2 = tg.generate({'type' => 'rebuild-full', 'exclude-not-testing' => true, 'include' => 'sphinx_and_docutils.txt', 'id' => 'sphinx-exp', 'modes' => ['sphinx-docutils-exp']  }) ;
puts JSON::pretty_generate(t1 + t2)" > tasks.json


ruby -Ilib -rcollab-qa -e "
tg = CollabQA::TasksGenerator::new ;
t1 = tg.generate({'type' => 'rebuild-full', 'exclude-not-testing' => true, 'include' => 'pytest.pkglist'}) ;
t2 = tg.generate({'type' => 'rebuild-full', 'exclude-not-testing' => true, 'include' => 'pytest.pkglist', 'modes' => ['pytest-exp'], 'id' => 'pytest-exp'})
tg12 = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'modes' => ['gcc12'], 'id' => 'gcc12'}) ;
tlto = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'modes' => ['dpkg-lto'], 'id' => 'dpkglto'}) ;
tu = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'id' => 'binarchonly'}) ;
puts JSON::pretty_generate(t1 + t2 + tg12 + tlto + tu)" > tasks.json

ruby -Ilib -rcollab-qa -e "
tg = CollabQA::TasksGenerator::new ;
tg12 = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'modes' => ['gcc12'], 'id' => 'gcc12'}) ;
tlto = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'modes' => ['dpkg-lto'], 'id' => 'dpkglto'}) ;
tu = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'id' => 'binarchonly'}) ;
tu2 = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'exclude' => '/tmp/res.OK.unstable'}) ;
puts JSON::pretty_generate(t14 + tg12 + tlto + tu + tu2)" > tasks.json

tg13 = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'modes' => ['gcc-exp'], 'id' => 'gccexp'}) ;
tlto = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'modes' => ['dpkg-lto'], 'id' => 'dpkglto'}) ;
tu = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'id' => 'binarchonly'}) ;
puts JSON::pretty_generate(tg13 + tlto + tu)" > tasks.json

ruby -Ilib -rcollab-qa -e "
tg = CollabQA::TasksGenerator::new ;
tsc = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'modes' => ['stackclash'], 'id' => 'stackclash'}) ;
tu = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'id' => 'binarchonly'}) ;
puts JSON::pretty_generate(tsc + tu)" > tasks.json


ruby -Ilib -rcollab-qa -e "
tg = CollabQA::TasksGenerator::new ;
t1 = tg.generate({'type' => 'rebuild-binarch-only-arm64', 'exclude-not-testing' => true}) ;
t2 = tg.generate({'type' => 'rebuild-binarch-only-arm64', 'exclude-not-testing' => true, 'modes' => ['branchprotection'], 'id' => 'branchprotection'}) ;
puts JSON::pretty_generate(t1 + t2)" > tasks.json

ruby -Ilib -rcollab-qa -e "
tg = CollabQA::TasksGenerator::new ;
t1 = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'include' => 'boost-packages'}) ;
t2 = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'include' => 'boost-packages', 'modes' => ['boost181'], 'id' => 'boost181'}) ;
puts JSON::pretty_generate(t1 + t2)" > tasks.json

ruby -Ilib -rcollab-qa -e "puts JSON::pretty_generate(CollabQA::TasksGenerator::new.generate({'type' => 'rebuild-full', 'exclude-not-testing' => true, 'exclude' => 'ok.1022'}))" > tasks.json

ruby -Ilib -rcollab-qa -e "puts JSON::pretty_generate(CollabQA::TasksGenerator::new.generate({'type' => 'rebuild-full', 'exclude-not-testing' => true}))" > tasks.json

ruby -Ilib -rcollab-qa -e "puts JSON::pretty_generate(CollabQA::TasksGenerator::new.generate({'type' => 'rebuild-full', 'exclude-not-testing' => true, 'exclude' => 'ok.1022'}))" > tasks.json

ruby -Ilib -rcollab-qa -e "puts JSON::pretty_generate(CollabQA::TasksGenerator::new.generate({'type' => 'rebuild-binarch-only', 'modes' => ['clang14'], 'id' => 'clang14', 'include' => '/tmp/todoclang'}))" > tasks.json

ruby -Ilib -rcollab-qa -e "puts JSON::pretty_generate(CollabQA::TasksGenerator::new.generate({'type' => 'rebuild-full', 'include' => 'retry'}))" > tasks.json

ruby -Ilib -rcollab-qa -e "
tg = CollabQA::TasksGenerator::new ;
t1 = tg.generate({'type' => 'rebuild-full', 'exclude-not-testing' => true, 'exclude' => '/tmp/OK.both'}) ;
puts JSON::pretty_generate(t1)" > tasks-normal.json

ruby -Ilib -rcollab-qa -e "
tg = CollabQA::TasksGenerator::new ;
t2 = tg.generate({'type' => 'rebuild-full', 'exclude-not-testing' => true, 'modes' => ['parallel'], 'id' => 'parallel', 'exclude' => '/tmp/OK.both'}) ;
puts JSON::pretty_generate(t2)" > tasks-parallel.json


ruby -Ilib -rcollab-qa -e "puts JSON::pretty_generate(CollabQA::TasksGenerator::new.generate({'type' => 'rebuild-full', 'exclude-not-testing' => true, 'exclude' => '/tmp/lOK'}))" > tasks.json

ruby -Ilib -rcollab-qa -e "
tg = CollabQA::TasksGenerator::new ;
t1 = tg.generate({'type' => 'rebuild-full', 'exclude-not-testing' => true}) ;
t2 = tg.generate({'type' => 'rebuild-full', 'exclude-not-testing' => true, 'include' => 'numpy.pkglist', 'modes' => ['numpy-exp'], 'id' => 'numpy-exp'}) ;
t3 = tg.generate({'type' => 'rebuild-full', 'exclude-not-testing' => true, 'include' => 'matplotlib.pkglist', 'modes' => ['matplotlib-exp'], 'id' => 'matplotlib-exp'}) ;
puts JSON::pretty_generate(t1 + t2 + t3)" > tasks.json

ruby -Ilib -rcollab-qa -e "
tg = CollabQA::TasksGenerator::new ;
t14 = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'modes' => ['clang14'], 'id' => 'clang14', 'exclude' => '/tmp/no_snek'}) ;
t15 = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'modes' => ['clang15'], 'id' => 'clang15', 'exclude' => '/tmp/no_snek'}) ;
tu = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'id' => 'binarchonly', 'exclude' => '/tmp/no_snek'}) ;
puts JSON::pretty_generate(t14 + t15 + tu)" > tasks.json

ruby -Ilib -rcollab-qa -e "
tg = CollabQA::TasksGenerator::new ;
t14 = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'modes' => ['clang14'], 'id' => 'clang14', 'exclude' => '/tmp/res.OK.unstable_clang14'}) ;
tg12 = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'modes' => ['gcc12'], 'id' => 'gcc12', 'exclude' => '/tmp/res.OK.unstable_gcc12'}) ;
tlto = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'modes' => ['dpkg-lto'], 'id' => 'dpkglto', 'exclude' => '/tmp/res.OK.unstable_dpkglto'}) ;
tu = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'id' => 'binarchonly', 'exclude' => '/tmp/res.OK.unstable_binarchonly'}) ;
tu2 = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'exclude' => '/tmp/res.OK.unstable'}) ;
puts JSON::pretty_generate(t14 + tg12 + tlto + tu + tu2)" > tasks.json

ruby -Ilib -rcollab-qa -e "puts JSON::pretty_generate(CollabQA::TasksGenerator::new.generate({'type' => 'rebuild-full-testing', 'exclude-not-testing' => true}))" > tasks.json




ruby -Ilib -rcollab-qa -e "puts JSON::pretty_generate(CollabQA::TasksGenerator::new.generate({'type' => 'rebuild-full', 'include' => 'failing'}))" > tasksf.json

ruby -Ilib -rcollab-qa -e "puts JSON::pretty_generate(CollabQA::TasksGenerator::new.generate({'type' => 'rebuild-full-arm64', 'exclude-not-testing' => true}))" > tasks.json

ruby -Ilib -rcollab-qa -e "puts JSON::pretty_generate(CollabQA::TasksGenerator::new.generate({'type' => 'rebuild-full-ppc64', 'exclude-not-testing' => true}))" > tasks.json

ruby -Ilib -rcollab-qa -e "puts JSON::pretty_generate(CollabQA::TasksGenerator::new.generate({'type' => 'rebuild-full-ppc64', 'exclude-not-testing' => true, 'exclude' => 'ppc64.OK'}))" > tasks.json


ruby -Ilib -rcollab-qa -e "
tg = CollabQA::TasksGenerator::new ;
t1 = tg.generate({'type' => 'rebuild-full', 'exclude-not-testing' => true}) ;
t2 = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'modes' => ['gcc11'], 'id' => 'gcc11'}) ;
t3 = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'id' => 'binarchonly'}) ;
puts JSON::pretty_generate(t1 + t2 + t3)" > tasks.json

ruby -Ilib -rcollab-qa -e "
tg = CollabQA::TasksGenerator::new ;
t = [] ;
t += tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true}) ;
t += tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'modes' => ['gcc10'], 'id' => 'gcc10'}) ;
t += tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'modes' => ['gcc10lto'], 'id' => 'gcc10lto'}) ;
t += tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'modes' => ['gcc11'], 'id' => 'gcc11'}) ;
t += tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'modes' => ['gcc10-v2'], 'id' => 'gcc10-v2'}) ;
t += tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'modes' => ['gcc10-v3'], 'id' => 'gcc10-v3'}) ;
puts JSON::pretty_generate(t)" > tasks.json

ruby -Ilib -rcollab-qa -e "
tg = CollabQA::TasksGenerator::new ;
t11 = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'modes' => ['clang11'], 'id' => 'clang11'}) ;
tu = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'id' => 'binarchonly'}) ;
puts JSON::pretty_generate(t11 + tu)" > tasks.json



ruby -Ilib -rcollab-qa -e "
tg = CollabQA::TasksGenerator::new ;
t1 = tg.generate({'type' => 'rebuild-full', 'exclude-not-testing' => true }) ;
t2 = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'id' => 'vanilla', 'include' => 'libboost-dev-rdeps' }) ;
t3 = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'id' => 'boost', 'include' => 'libboost-dev-rdeps', 'modes' => ['boost'] }) ;
puts JSON::pretty_generate(t1 + t2 + t3)" > tasks.json

ruby -Ilib -rcollab-qa -e "
tg = CollabQA::TasksGenerator::new ;
t1 = tg.generate({'type' => 'rebuild-full', 'exclude-not-testing' => true }) ;
t2 = tg.generate({'type' => 'rebuild-full', 'exclude-not-testing' => true, 'id' => 'ac270', 'modes' => ['autoconf2.70']  }) ;
t3 = tg.generate({'type' => 'rebuild-full', 'exclude-not-testing' => true, 'id' => 'fixfilepath', 'modes' => ['dpkg-reproducible-fixfilepath']  }) ;
puts JSON::pretty_generate(t1 + t2 + t3)" > tasks.json

ruby -Ilib -rcollab-qa -e "
tg = CollabQA::TasksGenerator::new ;
t1 = tg.generate({'type' => 'rebuild-full', 'exclude-not-testing' => true, 'include' => '/tmp/sphinx310.pkglist', 'id' => 'sphinx-ref' }) ;
t2 = tg.generate({'type' => 'rebuild-full', 'exclude-not-testing' => true, 'include' => '/tmp/sphinx310.pkglist', 'id' => 'sphinx-exp', 'modes' => ['sphinx-exp']  }) ;
t3 = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'id' => 'glibc-exp', 'modes' => ['glibc-exp']  }) ;
t4 = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true }) ;
puts JSON::pretty_generate(t1 + t2 + t3 + t4)" > tasks.json

ruby -Ilib -rcollab-qa -e "puts JSON::pretty_generate(CollabQA::TasksGenerator::new.generate({'type' => 'rebuild-full', 'exclude-not-testing' => true, 'exclude' => '/home/lucas/d/debian/collab-qa-tools/OK.20200222' }))" > tasks.json

ruby -Ilib -rcollab-qa -e "
tg = CollabQA::TasksGenerator::new ;
tn = tg.generate({'type' => 'rebuild-full', 'exclude-not-testing' => true}) ;
t10 = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'modes' => ['clang10'], 'id' => 'clang10'}) ;
tu = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'id' => 'binarchonly'}) ;
puts JSON::pretty_generate(tn + t10 + tu)" > tasks.json

ruby -Ilib -rcollab-qa -e "
tg = CollabQA::TasksGenerator::new ;
t1 = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true, 'modes' => ['gcc10'], 'id' => 'gcc10'}) ;
t2 = tg.generate({'type' => 'rebuild-binarch-only', 'exclude-not-testing' => true}) ;
puts JSON::pretty_generate(t1 + t2)" > tasks.json

ruby -Ilib -rcollab-qa -e "
tg = CollabQA::TasksGenerator::new ;
t1 = tg.generate({'type' => 'rebuild-full', 'exclude-not-testing' => true, 'include' => 'sphinx.pkglist', 'modes' => ['sphinx243'], 'id' => 'sphinx243'}) ;
t2 = tg.generate({'type' => 'rebuild-full', 'exclude-not-testing' => true, 'include' => 'sphinx.pkglist'}) ;
puts JSON::pretty_generate(t1 + t2)" > tasks.json



ruby -Ilib -rcollab-qa -e "puts JSON::pretty_generate(CollabQA::TasksGenerator::new.generate({'type' => 'rebuild-full-testing', 'exclude-not-testing' => true}))" > tasks.json

ruby -Ilib -rcollab-qa -e "
tg = CollabQA::TasksGenerator::new ;
t1 = tg.generate({'type' => 'rebuild-binarch-only', 'modes' => ['gcc9'], 'id' => 'gcc9'}) ;
t2 = tg.generate({'type' => 'rebuild-binarch-only'}) ;
t3 = tg.generate({'type' => 'rebuild-full-testing', 'exclude-not-testing' => true}) ;
puts JSON::pretty_generate(t1 + t2 + t3)" > tgc.json

ruby -Ilib -rcollab-qa -e "t1 = CollabQA::TasksGenerator::new.generate({'type' => 'rebuild-full-testing', 'exclude-not-testing' => true}) ;
t1.select! { |e| e['esttime'] && e['esttime'] < 30 }
t1 = t1.shuffle[0..100]
puts JSON::pretty_generate(t1)" > tshort.json

ruby -Ilib -rcollab-qa -e "t1 = CollabQA::TasksGenerator::new.generate({'type' => 'rebuild-full-testing', 'exclude-not-testing' => true}) ;
t1.select! { |e| e['esttime'] && e['esttime'] < 30 }
t1 = t1.shuffle[0..100]
puts JSON::pretty_generate(t1)" > tshort.json

ruby -Ilib -rcollab-qa -e "
t1 = CollabQA::TasksGenerator::new.generate({'type' => 'rebuild-binarch-only', 'modes' => ['gcc9'], 'id' => 'gcc9'}) ;
t2 = CollabQA::TasksGenerator::new.generate('{type' => ) ;
t1.select! { |e| e['esttime'] && e['esttime'] < 30 }
t1 = t1.shuffle[0..20]
puts JSON::pretty_generate(t1)" > tshort.json

================= Examples below need to be ported to the new separation =================

Basic examples:
ruby -Ilib -rcollab-qa -e "puts JSON::pretty_generate(CollabQA::TasksGenerator::new('rebuild-full').generate({'exclude-not-testing' => true, 'include' => '/tmp/failed'}))"

ruby -Ilib -rcollab-qa -e "puts JSON::pretty_generate(CollabQA::TasksGenerator::new('rebuild-full').generate({'exclude-not-testing' => true, 'exclude' => '/home/lucas/d/debian/collab-qa-tools/ok.2612'}))"

ruby -Ilib -rcollab-qa -e "puts JSON::pretty_generate(CollabQA::TasksGenerator::new('rebuild-full').generate({'exclude-not-testing' => true}))"

ruby -Ilib -r collab-qa -e "puts JSON::pretty_generate(CollabQA::TasksGenerator::new('rebuild-full').generate({'exclude' => '/home/lucas/res.OK'}))"

ruby -Ilib -rcollab-qa -e "puts JSON::pretty_generate(CollabQA::TasksGenerator::new('rebuild-full-testing').generate)"

ruby -Ilib -rcollab-qa -e "t1 = CollabQA::TasksGenerator::new('rebuild-full').generate({'modes' => ['parallel']}) ; puts JSON::pretty_generate(t1)"

ruby -Ilib -rcollab-qa -e "puts JSON::pretty_generate(CollabQA::TasksGenerator::new('rebuild-arch-all-only').generate)"

Arch-all only:
ruby -Ilib -rcollab-qa -e "puts JSON::pretty_generate(CollabQA::TasksGenerator::new('rebuild-arch-all-only').generate({'include' => 'failed.archallonly', 'id' => 'archallonly'}))" > archallonly.json
ruby -Ilib -rcollab-qa -e "puts JSON::pretty_generate(CollabQA::TasksGenerator::new('rebuild-full').generate({'include' => 'failed.archallonly', 'id' => 'classic'}))" > classic.json

# With pie/bindnow tweaks
'modes' => ['pie-bindnow']

ruby -Ilib -rcollab-qa -e "t1 = CollabQA::TasksGenerator::new('rebuild-binarch-only').generate({'modes' => ['clang'], 'id' => 'clang3.8'}) ; t2 = CollabQA::TasksGenerator::new('rebuild-binarch-only').generate({'id' => 'pie-normal', 'include' => 'failed.pie'}) ; puts JSON::pretty_generate(t1 + t2)"


ruby -Ilib -rcollab-qa -e "t1 = CollabQA::TasksGenerator::new('rebuild-full').generate({'include' => '/tmp/failed0906'}) ; t2 = CollabQA::TasksGenerator::new('rebuild-full-i386').generate({'include' => '/tmp/failed0906'}) ; puts JSON::pretty_generate(t1+t2)" > full.json

ruby -Ilib -rcollab-qa -e "
t1 = CollabQA::TasksGenerator::new('rebuild-full-arm64').generate({'include' => 'times.120'}) ;
t2 = CollabQA::TasksGenerator::new('rebuild-full-armhf').generate({'include' => 'times.120'}) ;
t3 = CollabQA::TasksGenerator::new('rebuild-full-armel').generate({'include' => 'times.120'}) ;
puts JSON::pretty_generate(t1+t2+t3)" > full.json

ruby -Ilib -rcollab-qa -e "
t1 = CollabQA::TasksGenerator::new('rebuild-full').generate({'modes' => ['gcc-unstable'], 'id' => 'gcc7'}) ;
t2 = CollabQA::TasksGenerator::new('rebuild-full').generate ;
puts JSON::pretty_generate(t1 + t2)" > full.json

ruby -Ilib -rcollab-qa -e "
t1 = CollabQA::TasksGenerator::new('rebuild-full-testing-i386').generate({'include' => 'failed.testing'}) ;
t2 = CollabQA::TasksGenerator::new('rebuild-full-testing').generate({'include' => 'failed.testing'}) ;
puts JSON::pretty_generate(t1 + t2)" > full.json

ruby -Ilib -rcollab-qa -e "
t1 = CollabQA::TasksGenerator::new('rebuild-full-testing-i386').generate() ;
t2 = CollabQA::TasksGenerator::new('rebuild-full-testing').generate() ;
puts JSON::pretty_generate(t1 + t2)" > full.json


ruby -Ilib -rcollab-qa -e "
t1 = CollabQA::TasksGenerator::new('rebuild-binarch-only').generate({'modes' => ['clang'], 'id' => 'clang4.0'}) ;
t3 = CollabQA::TasksGenerator::new('rebuild-binarch-only').generate ;
puts JSON::pretty_generate(t1 + t2 + t3)"

ruby -Ilib -rcollab-qa -e "
t1 = CollabQA::TasksGenerator::new('rebuild-binarch-only').generate({'modes' => ['clang60'], 'id' => 'clang60'}) ;
t2 = CollabQA::TasksGenerator::new('rebuild-binarch-only').generate({'modes' => ['glibc-exp'], 'id' => 'glibc-exp'}) ;
t3 = CollabQA::TasksGenerator::new('rebuild-binarch-only').generate ;
puts JSON::pretty_generate(t1 + t2 + t3)" > all.json

ruby -Ilib -rcollab-qa -e "
t1 = CollabQA::TasksGenerator::new('rebuild-binarch-only').generate({'modes' => ['clang50'], 'id' => 'clang5.0'}) ;
t2 = CollabQA::TasksGenerator::new('rebuild-binarch-only').generate() ;
puts JSON::pretty_generate(t1 + t2)" > all.json


ruby -Ilib -rcollab-qa -e "
t1 = CollabQA::TasksGenerator::new('rebuild-full').generate({'include' => '/home/lucas/pkgs'}) ;
puts JSON::pretty_generate(t1)" > ruby.json

ruby -Ilib -rcollab-qa -e "
t1 = CollabQA::TasksGenerator::new('rebuild-binarch-only').generate({'modes' => ['gcc9'], 'id' => 'gcc9'}) ;
t2 = CollabQA::TasksGenerator::new('rebuild-binarch-only').generate({'modes' => ['clang7'], 'id' => 'clang7'}) ;
t3 = CollabQA::TasksGenerator::new('rebuild-binarch-only').generate({'modes' => ['clang8'], 'id' => 'clang8'}) ;
t4 = CollabQA::TasksGenerator::new('rebuild-binarch-only').generate() ;
puts JSON::pretty_generate(t1 + t2 + t3 + t4)" > tgc.json

ruby -Ilib -rcollab-qa -e "
t1 = CollabQA::TasksGenerator::new('rebuild-binarch-only').generate({'modes' => ['gcc9'], 'id' => 'gcc9'}) ;
t2 = CollabQA::TasksGenerator::new('rebuild-binarch-only').generate({'modes' => ['clang7'], 'id' => 'clang7'}) ;
t3 = CollabQA::TasksGenerator::new('rebuild-binarch-only').generate({'modes' => ['clang8'], 'id' => 'clang8'}) ;
t4 = CollabQA::TasksGenerator::new('rebuild-binarch-only').generate() ;
puts JSON::pretty_generate(t1 + t2 + t3 + t4)" > tgc.json



ruby -Ilib -rcollab-qa -e "t1 = CollabQA::TasksGenerator::new('rebuild-full').generate({'exclude-not-testing' => true}) ;
t1.select! { |e| e['esttime'] && e['esttime'] < 30 }
t1 = t1.shuffle[0..100]
puts JSON::pretty_generate(t1)" > tshort.json
=end

require 'open-uri'
require 'pp'
require 'json'

BUILDTIME_LIST = File.join(File.expand_path(File.dirname(__FILE__)), '../../data/buildtime.list')
SOURCES_FACTS = 'https://udd.debian.org/cgi-bin/sources_facts.cgi'
#SOURCES_FACTS = 'sources_facts.cgi'

BLACKLISTED_SOURCES = [
  'signon-ui', # #805389
  'gcc-snapshot' # takes ages + never will be part of testing
]

TESTING='trixie'

module CollabQA
  class TasksGenerator
    def initialize

      @buildtime = Hash[IO::readlines(BUILDTIME_LIST).map { |l| l.split }.map { |e| [ e[0], e[1].to_i ] }]

      @sources = JSON::parse(URI.open(SOURCES_FACTS, :read_timeout => 120).read)
    end

    def generate(opts = {})
      type = opts['type']
      chroot = 'unstable'

      id = opts['id'] if opts['id']
      modes = opts['modes'] if opts['modes']
      modes ||= []
      if opts['include']
        inc = IO::readlines(opts['include']).map { |l| l.chomp }
      else
        inc = nil
      end
      if opts['exclude']
        exclude = IO::readlines(opts['exclude']).map { |l| l.chomp }
      else
        exclude = nil
      end

      rebuild_sources = @sources.to_a
      rebuild_sources.select! { |d| _, e = d ; e['sid'] and not e['sid']['extra_source_only'] }
      rebuild_sources.select! { |d| _, e = d; e['sid'] and e['sid']['component'] == 'main' }

      case type
      when 'rebuild-full'
        rebuild_sources = rebuild_sources.select { |d| _, e = d ; (e['pas'].nil? or e['pas'].include?('amd64') ) and (e['sid'] and e['sid']['architecture'] & ['amd64', 'all', 'any', 'linux-any'] != []) }
      when 'rebuild-full-ppc64'
        rebuild_sources = rebuild_sources.select { |d| _, e = d ; (e['pas'].nil? or e['pas'].include?('ppc64') ) and (e['sid'] and e['sid']['architecture'] & ['ppc64', 'all', 'any', 'linux-any'] != []) }
      when 'rebuild-full-i386'
        rebuild_sources = rebuild_sources.select { |d| _, e = d ; (e['pas'].nil? or e['pas'].include?('i386') ) and (e['sid'] and e['sid']['architecture'] & ['i386', 'all', 'any', 'linux-any'] != []) }
        chroot = 'unstable-i386'
      when 'rebuild-full-testing'
        rebuild_sources = rebuild_sources.select { |d| _, e = d ; (e['pas'].nil? or e['pas'].include?('amd64') ) and (e[TESTING] and e[TESTING]['architecture'] & ['amd64', 'all', 'any', 'linux-any'] != []) }
        chroot = 'testing'
      when 'rebuild-full-testing-i386'
        rebuild_sources = rebuild_sources.select { |d| _, e = d ; (e['pas'].nil? or e['pas'].include?('i386') ) and (e[TESTING] and e[TESTING]['architecture'] & ['i386', 'all', 'any', 'linux-any'] != []) }
        chroot = 'testing-i386'
      when 'rebuild-arch-all-only'
        rebuild_sources = rebuild_sources.select { |d| _, e = d ; (e['sid'] and e['sid']['architecture'] & [ 'all' ] != []) }
        modes << 'arch-all-only'
      when 'rebuild-binarch-only'
        rebuild_sources = rebuild_sources.select { |d| _, e = d ; (e['pas'].nil? or e['pas'].include?('amd64') ) and (e['sid'] and e['sid']['architecture'] & ['amd64', 'any', 'linux-any'] != []) }
        modes << 'binarch-only'
      when 'rebuild-binarch-only-arm64'
        rebuild_sources = rebuild_sources.select { |d| _, e = d ; (e['pas'].nil? or e['pas'].include?('arm64') ) and (e['sid'] and e['sid']['architecture'] & ['arm64', 'any', 'linux-any'] != []) }
        modes << 'binarch-only'
      when 'rebuild-full-arm64'
        rebuild_sources = rebuild_sources.select { |d| _, e = d ; (e['pas'].nil? or e['pas'].include?('arm64') ) and (e['sid'] and e['sid']['architecture'] & ['arm64', 'all', 'any', 'linux-any'] != []) }
        chroot = 'unstable-arm64'
      when 'rebuild-full-armhf'
        rebuild_sources = rebuild_sources.select { |d| _, e = d ; (e['pas'].nil? or e['pas'].include?('armhf') ) and (e['sid'] and e['sid']['architecture'] & ['armhf', 'all', 'any', 'linux-any'] != []) }
        chroot = 'unstable-armhf'
      when 'rebuild-full-armel'
        rebuild_sources = rebuild_sources.select { |d| _, e = d ; (e['pas'].nil? or e['pas'].include?('armel') ) and (e['sid'] and e['sid']['architecture'] & ['armel', 'all', 'any', 'linux-any'] != []) }
        chroot = 'unstable-armel'
      else
        raise "Unknown type #{type}"
      end

      if opts['exclude-not-testing']
        rebuild_sources = rebuild_sources.select { |d| _, e = d; e[TESTING] != nil }
      end

      rebuild_sources = rebuild_sources.select { |e| not BLACKLISTED_SOURCES.include?(e[0]) }

      if inc
        rebuild_sources = rebuild_sources.select { |e| inc.include?(e[0]) }
      end
      if exclude
        rebuild_sources = rebuild_sources.select { |e| not exclude.include?(e[0]) }
      end
      
      # FIXME add support for maxtime/mintime
      tasks = []
      rebuild_sources.each do |e|
        src, v = e
        t = {}
        t['type'] = type
        t['source'] = src
        if ['rebuild-full-testing', 'rebuild-full-testing-i386'].include?(type)
          t['version'] = v[TESTING]['version']
        else
          t['version'] = v['sid']['version']
        end
        t['chroot'] = chroot
        t['esttime'] = @buildtime[src] || nil
        logversion = t['version'].gsub(/^[0-9]+:/,'')
        if id
          t['logfile'] = "/tmp/#{t['source']}_#{logversion}_#{t['chroot']}_#{id}.log"
        else
          t['logfile'] = "/tmp/#{t['source']}_#{logversion}_#{t['chroot']}.log"
        end
        t['modes'] = modes || []
        tasks << t
      end
      return tasks
    end
  end
end
