#!/usr/bin/ruby -w

$:.unshift File.join(File.dirname(__FILE__), '..', 'lib')

require 'minitest'
require 'collab-qa'

class LogsTest < Minitest::Test
  if File::directory?('test/source')
    SRCDIR = 'test/source'
    DSTDIR = 'test/dest'
  elsif File::directory?('source')
    SRCDIR = 'source'
    DSTDIR = 'dest'
  else
    raise 'source directory not found.'
  end

  Dir.foreach(SRCDIR).sort.each do |f|
    next if f !~ /log$/
    next if ENV['SOURCE'] != nil and ENV['SOURCE'] != f
    testname = "test_" + File.basename(f).gsub(/[^\w]/, "_")
    define_method(testname) do
      log = CollabQA::Log::new(SRCDIR + '/' + f)
      log.guess_failed
      log.extract_log
      str = log.to_s
      if ENV['CREATE_OUTPUT']
        File::open(DSTDIR + '/' + f.gsub(/log$/, 'output'), 'w') { |fd| fd.puts str }
      end
      output = File::read(DSTDIR + '/' + f.gsub(/log$/, 'output'))
      assert_equal str, output
    end
  end
end
